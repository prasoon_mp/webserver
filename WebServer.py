import socket
import os
import subprocess



class HttpMessage:
	requestList=""
	dict={}
	
		
	def firstLine(self,fl):
		
		return fl
		
	def connection(self):
		return 'close'
			
	def contentLength(self,cLength):
		return cLength
		
	def contentType(self,ctype):
		return ctype

		
class Request(HttpMessage):
	
	def __init__(self,attr):
		self.requestList=attr
		#print self.parseFirstLine()
		
	def decodeUrl(self,url):
		
		return url	
		
	def parseFirstLine(self):
		self.dictFirstLine={}
		self.dictParameter={}
		firstLineList=self.requestList[0].split(' ')
		self.dictFirstLine['requestType']=firstLineList[0]
		self.dictFirstLine['url']=self.decodeUrl(firstLineList[1])
		
		url=firstLineList[1].split('?')
		self.dictFirstLine['fileName']=url[0]
		self.dictFirstLine['mimeType']=url[0].split('.')[1]
		if firstLineList[1].find("?")!=-1:
			self.dictFirstLine['parameters']=url[1]
		
		self.dictFirstLine['httpVersion']=firstLineList[2]
		
		if self.dictFirstLine['requestType'] in {"GET","POST"} and self.dictFirstLine['url']!="" and self.dictFirstLine['httpVersion'] in {'HTTP/1.1','HTTP/1.0','HTTP/0.9'} :
			return self.dictFirstLine
		else :
			raise ErrorHandler("500")
		
	def unserialize(self):
		for content in self.requestList:
			
			if content.find('GET') == -1 and content.find('POST') == -1 and content.find(':')!=-1:
				lst=content.split(':', 1 )
				self.dict[lst[0]]=lst[1]
				
		return self.dict
	
		
class Response(HttpMessage):
	
	def __init__(self,attr,cs):
		self.cs=cs
		self.genResponse(attr,cs)
			
	def genResponse(self,attr,cs): 
		dictFirstLine=attr
		nl='\r\n'
		imageExt={'png':'png','jpg':'jpeg','gif':'gif'}
		fileExt={'html':'html','php':'html'}
		ext=dictFirstLine['mimeType']
		
		if ext in imageExt:
			mimeType='image/'+imageExt[ext]
		elif ext in fileExt:
			mimeType='text/'+fileExt[ext]
		else: mimeType='application/octet-stream'
		
		self.response = dictFirstLine['httpVersion']+' 200 OK'+nl+'Content-type:'+mimeType+nl+'Connection:close'
				
		self.fileResponse=FileHandler(dictFirstLine['fileName'])
		self.send(self.response,self.fileResponse,cs)
				
	def send(self,response,fileResponse,cs):
		print fileResponse
		
		cs.sendall(response+'\r\n\r\n')
		cs.close()
		


class FileHandler:
	def __init__(self,fileName):
		self.processFile(fileName)
		
	def processFile(self,fileName):
		try:
			ext=fileName.split('.')[1]
			if ext=='php':
				proc = subprocess.Popen("php "+(os.path.dirname(os.path.abspath(__file__))+ dictFirstLine['fileName']), shell=True, stdout=subprocess.PIPE)
				self.fileResponse = proc.stdout.read()
			else:
				proc = open(os.path.dirname(os.path.abspath(__file__))+ fileName)
				self.fileResponse =proc.read()
			print self.fileResponse
			return self.fileResponse	
		except IOError:
			raise ErrorHandler("404")
			
		
	def fileLength():
		return
	

class Web_server:
	def __init__(self):
		try:
			HOST = ''
			PORT = 8080
			self.ss = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			self.ss.bind((HOST, PORT))
			self.ss.listen(1)
			
			print 'Server started ...'
			
			
						
		except KeyboardInterrupt:
			
			self.ss.close()
			print  'shutting down server'
				
	def run(self):
		self.req=''
		while True:
			self.cs, addr = self.ss.accept()
			self.req=''
			self.readRequest()
					
			
	def readRequest(self):
		header=''
		data=""
		while True:
			data += self.cs.recv(5024)
			if data.find('\r\n\r\n') != -1:
				self.req = data.split('\r\n')
				break
		
		HttpHandler(self.req,self.cs)
		#return 	self.req				
			
class HttpHandler:
	def __init__(self,req,cs):
		
		self.processRequest(req,cs)
		print cs
	
		
	def processRequest(self,req,cs):
		try:
			
			obReq=Request(req)
			dictFirstLine=obReq.parseFirstLine()
			#print dictFirstLine
			dictUnserialize=obReq.unserialize()
			Response(dictFirstLine,cs)
			
		
		except ErrorHandler,e:
			print e.response
			cs.sendall(e.response)
			cs.close()
			
class ErrorHandler(Exception):
	def __init__(self,error):
		
		self.processError(error)
		#self.response
		
		print error
	def genErrorResponse(self,errorCode):
		if errorCode=='404':
			return "HTTP/1.0 404 Not Found \r\nContent-type:text/plain\r\n\r\nFile Not Found"
		if errorCode=='500':
			return "HTTP/1.0 500 Internal Server Error \r\nContent-type:text/plain\r\n\r\nInternal Server Error"
	
		
	def processError(self,error):
		self.response=self.genErrorResponse(error)
		#print response
		return self.response
			
	
		
if __name__ == '__main__':
    Web_server().run()
    
	


 
  
